## [1.6.2](https://gitlab.com/to-be-continuous/cloud-foundry/compare/1.6.1...1.6.2) (2022-01-10)


### Bug Fixes

* non-blocking warning in case failed decoding [@url](https://gitlab.com/url)@ variable ([68a4eb4](https://gitlab.com/to-be-continuous/cloud-foundry/commit/68a4eb46afc0bdd0d8e486e7542292708f3cd698))

## [1.6.1](https://gitlab.com/to-be-continuous/cloud-foundry/compare/1.6.0...1.6.1) (2021-10-07)


### Bug Fixes

* use master or main for production env ([8dfeb49](https://gitlab.com/to-be-continuous/cloud-foundry/commit/8dfeb49b717c763b845fa56aa9567bdafea2facc))

## [1.6.0](https://gitlab.com/to-be-continuous/cloud-foundry/compare/1.5.1...1.6.0) (2021-09-02)

### Features

* Change boolean variable behaviour ([420b471](https://gitlab.com/to-be-continuous/cloud-foundry/commit/420b471087ffe8fe7b81ae643032564486b501d1))

## [1.5.1](https://gitlab.com/to-be-continuous/cloud-foundry/compare/1.5.0...1.5.1) (2021-08-31)

### Bug Fixes

* **bluegreen:** handle path when mapping routes ([e2219b8](https://gitlab.com/to-be-continuous/cloud-foundry/commit/e2219b8c8bc7325e767654dd4d982e9377c6cacb))

## [1.5.0](https://gitlab.com/to-be-continuous/cloud-foundry/compare/1.4.4...1.5.0) (2021-07-27)

### Features

* **hooks:** support post bluegreen hook ([7f7c0e4](https://gitlab.com/to-be-continuous/cloud-foundry/commit/7f7c0e4f43825184702cd7e69f7748b542f1f061))

## [1.4.4](https://gitlab.com/to-be-continuous/cloud-foundry/compare/1.4.3...1.4.4) (2021-07-26)

### Bug Fixes

* **keep-retired-app:** dont retire unexisting app and unmap routes with correct path ([7ec512f](https://gitlab.com/to-be-continuous/cloud-foundry/commit/7ec512f6d0fb58d1e16a4306c32dfe32b671c794))

## [1.4.3](https://gitlab.com/to-be-continuous/cloud-foundry/compare/1.4.2...1.4.3) (2021-07-08)

### Bug Fixes

* conflict between vault and scoped vars ([85f1ed6](https://gitlab.com/to-be-continuous/cloud-foundry/commit/85f1ed68e49d26b282468666ac6f9c5051ac2be3))

## [1.4.2](https://gitlab.com/to-be-continuous/cloud-foundry/compare/1.4.1...1.4.2) (2021-07-07)

### Bug Fixes

* **keep-retired-app:** exclude path on domain parsing ([e11f0b2](https://gitlab.com/to-be-continuous/cloud-foundry/commit/e11f0b2a677ebf204ec250b0ed36f15aa74c6dc0))

## [1.4.1](https://gitlab.com/to-be-continuous/cloud-foundry/compare/1.4.0...1.4.1) (2021-06-23)

### Bug Fixes

* stop retired app ([1be9cab](https://gitlab.com/to-be-continuous/cloud-foundry/commit/1be9cab18007d1b1d8f6ae408cca39c47e700426))

## [1.4.0](https://gitlab.com/to-be-continuous/cloud-foundry/compare/1.3.0...1.4.0) (2021-06-19)

### Features

* support multi-lines environment variables substitution ([db9f645](https://gitlab.com/to-be-continuous/cloud-foundry/commit/db9f6454a3a955182f401b44173305595846f5fc))

## [1.3.0](https://gitlab.com/to-be-continuous/cloud-foundry/compare/1.2.1...1.3.0) (2021-06-16)

### Features

* allow to keep old version on deploy ([62c1a96](https://gitlab.com/to-be-continuous/cloud-foundry/commit/62c1a962f9595ac626012cab63a8eac04bf8475d))

## [1.2.1](https://gitlab.com/to-be-continuous/cloud-foundry/compare/1.2.0...1.2.1) (2021-06-14)

### Bug Fixes

* **bluegreen:** save failed start app logs ([5adec5f](https://gitlab.com/to-be-continuous/cloud-foundry/commit/5adec5f2ed0a6e9bc2a31017ab370e0d11514591))

## [1.2.0](https://gitlab.com/to-be-continuous/cloud-foundry/compare/1.1.0...1.2.0) (2021-06-10)

### Features

* move group ([12c0340](https://gitlab.com/to-be-continuous/cloud-foundry/commit/12c03407fe1fefeccfd9205c98aaf5c160a95669))

## [1.1.0](https://gitlab.com/Orange-OpenSource/tbc/cloud-foundry/compare/1.0.1...1.1.0) (2021-05-18)

### Features

* add scoped variables support ([c124736](https://gitlab.com/Orange-OpenSource/tbc/cloud-foundry/commit/c1247364e2dbe0ec72ae4219bfeb6bc8329bae31))

## [1.0.1](https://gitlab.com/Orange-OpenSource/tbc/cloud-foundry/compare/1.0.0...1.0.1) (2021-05-11)

### Bug Fixes

* environment url not propagated in blue green when app does not exist ([87ebbb6](https://gitlab.com/Orange-OpenSource/tbc/cloud-foundry/commit/87ebbb60fa2d7230f047a251e6d350cf97408730))

## 1.0.0 (2021-05-06)

### Features

* initial release ([709e0d4](https://gitlab.com/Orange-OpenSource/tbc/cloud-foundry/commit/709e0d47bbfb17487e1ffb17e64deb0e783990c5))
